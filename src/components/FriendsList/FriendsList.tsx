import React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardHeader from '@mui/material/CardHeader';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { useTranslation } from "react-i18next";
import { connect } from 'react-redux';

const FriendsList = (props : any) => {
  const { t } = useTranslation();
  return (
    <React.Fragment>
      {/* Hero unit */}
      <Container disableGutters maxWidth="sm" component="main" sx={{ pt: 8, pb: 6 }}>
        <Typography
          component="h1"
          variant="h2"
          align="center"
          color="text.primary"
          gutterBottom
        >
          {t('all_friends')}
        </Typography>
      </Container>
      {/* End hero unit */}
      <Container maxWidth="md" component="main">
        <Grid container spacing={5} alignItems="flex-end">
          {props.users.map((user:any) => (
            // Enterprise card is full width at sm breakpoint

            props.searchString !== '' ? 
            ((user.name.indexOf(props.searchString) > 0) ? 
            (user.friend ? <Grid
              item
              key={user.id}
              xs={12}
              md={4}
            >
              <Card>
                <CardHeader
                  title={user.name}
                  subheader={user.username}
                  titleTypographyProps={{ align: 'left' }}
                  subheaderTypographyProps={{
                    align: 'left',
                  }}
                />
                <CardContent>
                  <Box
                    sx={{
                      display: 'flex',
                      justifyContent: 'left',
                      alignItems: 'baseline',
                      mb: 2,
                    }}
                  >
                    <Typography>
                      Email : {user.email}
                    </Typography>
                  </Box>
                </CardContent>
                <CardActions>
                  {user.friend ?
                    <Button
                      fullWidth
                      variant='outlined'
                    > Added </Button> :
                    <Button
                      fullWidth
                      variant='outlined'
                    > Add to Friend </Button>
                  }
                </CardActions>
              </Card>
            </Grid> : ''): '') 
            : (user.friend ? <Grid
              item
              key={user.id}
              xs={12}
              md={4}
            >
              <Card>
                <CardHeader
                  title={user.name}
                  subheader={user.username}
                  titleTypographyProps={{ align: 'left' }}
                  subheaderTypographyProps={{
                    align: 'left',
                  }}
                />
                <CardContent>
                  <Box
                    sx={{
                      display: 'flex',
                      justifyContent: 'left',
                      alignItems: 'baseline',
                      mb: 2,
                    }}
                  >
                    <Typography>
                      Email : {user.email}
                    </Typography>
                  </Box>
                </CardContent>
                <CardActions>
                  {user.friend ?
                    <Button
                      fullWidth
                      variant='outlined'
                    > Added </Button> :
                    <Button
                      fullWidth
                      variant='outlined'
                    > Add to Friend </Button>
                  }
                </CardActions>
              </Card>
            </Grid> : '')
            ))}
        </Grid>
      </Container>
    </React.Fragment>
  );
}

const mapStateToProps = (state: any) => {
  console.log(state);
  return {
    age: state.age,
    showSpinner: state.showSpinner,
    users : state.users,
    userId : state.userId,
    title:state.title,
    searchString : state.searchString
  };
};

export default connect(
  mapStateToProps
)(FriendsList);