import React from 'react';
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import CssBaseline from '@mui/material/CssBaseline';
import GlobalStyles from '@mui/material/GlobalStyles';
import { useTranslation } from "react-i18next";

import Header from '../../pages/Header';
import UsersList from '../UsersList/UsersList';
import FriendsList from '../FriendsList/FriendsList';


function App() {

  const { t } = useTranslation();

  return (
    <React.Fragment>
        <Router>
          <GlobalStyles styles={{ ul: { margin: 0, padding: 0, listStyle: 'none' } }} />
          <CssBaseline />
          <Header />
          <Routes>
            <Route path="/friends" element={<FriendsList title={t('all_friends')} />} />
            <Route path="/" element={<UsersList title={t('all_users')}/>} />
          </Routes>
        </Router>
    </React.Fragment>
  );
}

export default App;