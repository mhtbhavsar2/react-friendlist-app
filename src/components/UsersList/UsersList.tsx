import React, { useEffect, useRef } from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardHeader from '@mui/material/CardHeader';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { CircularProgress, Fade } from '@mui/material';
import { connect, useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';

const UsersList = (props: any) => {
  const dispatch = useDispatch();
  const timerRef = useRef(0);
  useEffect(() => {
    if (props.users.length <= 0) {
      dispatch({ type: 'USER_FETCH_REQUESTED' })
    }
  });

  useEffect(() => {
    // clearTimeout(timerRef.current);
  }, []);


  const showSpinnerById = (id: string, spinnerStatus: string) => {
    console.log('showSpinnerById - props.showSpinner -', props.showSpinner);
    if (timerRef.current) {
      clearTimeout(timerRef.current);
    }

    if (spinnerStatus == 'idle')
      dispatch({ type: "SHOW_SPINNER", value: { userid: id, showSpinner: 'pending' } })
    timerRef.current = window.setTimeout(() => {
      dispatch({ type: "SHOW_SPINNER", value: { userid: id, showSpinner: 'idle' } })
    }, 2000);

    console.log('end of showSpinnerById - props.showSpinner -', props.showSpinner);
    return;

  };

  const { t } = useTranslation();
  return (
    <React.Fragment>
      {/* Hero unit */}
      <Container disableGutters maxWidth="sm" component="main" sx={{ pt: 8, pb: 6 }}>
        <Typography
          component="h1"
          variant="h2"
          align="center"
          color="text.primary"
          gutterBottom
        >
          {t('all_users')}
        </Typography>
      </Container>
      {/* End hero unit */}

      <Container maxWidth="md" component="main">
        <Grid container spacing={5} alignItems="flex-end">
          {props.users.map((user: any) => (
            // Enterprise card is full width at sm breakpoint
            props.searchString !== '' ?
              (user.name.indexOf(props.searchString) > 0) ?
                <Grid
                  item
                  key={user.id}
                  xs={12}
                  md={4}
                >
                  <Card>
                    <CardHeader
                      title={user.name}
                      subheader={user.username}
                      titleTypographyProps={{ align: 'center' }}
                      subheaderTypographyProps={{
                        align: 'center',
                      }}
                    />
                    <CardContent>
                      <Box
                        sx={{
                          display: 'flex',
                          justifyContent: 'center',
                          alignItems: 'baseline',
                          mb: 2,
                        }}
                      >
                        <Typography>
                          {user.email}
                        </Typography>
                      </Box>
                    </CardContent>
                    <CardActions>
                      {user.circularProgressStatus === 'success' ? <Button
                        fullWidth
                        variant='outlined'
                      >{props.userId === user.id && props.showSpinner === 'pending' ? <CircularProgress /> : 'Added'}</Button> :
                        <Button
                          fullWidth
                          variant='outlined'
                          onClick={(e) => { console.log("before click on addToFriend - ", props); showSpinnerById(props.userId, props.showSpinner); return props.addToFriend(user.id) }}
                        > Add to Friend
                        </Button>
                      }
                    </CardActions>
                  </Card>
                </Grid> : '' :
              <Grid
                item
                key={user.id}
                xs={12}
                md={4}
              >
                <Card>
                  <CardHeader
                    title={user.name}
                    subheader={user.username}
                    titleTypographyProps={{ align: 'center' }}
                    subheaderTypographyProps={{
                      align: 'center',
                    }}
                  />
                  <CardContent>
                    <Box
                      sx={{
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'baseline',
                        mb: 2,
                      }}
                    >
                      <Typography>
                        {user.email}
                      </Typography>
                    </Box>
                  </CardContent>
                  <CardActions>
                    {user.circularProgressStatus === 'success' ? <Button
                      fullWidth
                      variant='outlined'
                    >{props.userId === user.id && props.showSpinner === 'pending' ? <CircularProgress /> : 'Added'}</Button> :
                      <Button
                        fullWidth
                        variant='outlined'
                        onClick={(e) => { console.log("before click on addToFriend - ", props); showSpinnerById(props.userId, props.showSpinner); return props.addToFriend(user.id) }}
                      > Add to Friend
                      </Button>
                    }
                  </CardActions>
                </Card>
              </Grid>
          ))}
        </Grid>
      </Container>
    </React.Fragment>
  );
}

const mapStateToProps = (state: any) => {
  return {
    age: state.age,
    showSpinner: state.showSpinner,
    users: state.users,
    userId: state.userId,
    title: state.title,
    searchString: state.searchString
  };
};

const mapDispachToProps = (dispatch: any) => {
  return {
    addToFriend: (d: string) => { console.log('addToFriend - ', d); return dispatch({ type: "ADD_FRIEND", value: d }) }
  };
};
export default connect(
  mapStateToProps,
  mapDispachToProps
)(UsersList);