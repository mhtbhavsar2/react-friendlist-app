import axios from "axios";
import { IUser } from "../redux/sagas";

export const getUsers = () => axios.get<IUser[]>("https://jsonplaceholder.typicode.com/users");
