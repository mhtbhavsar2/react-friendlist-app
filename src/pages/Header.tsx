import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import { Link as RouterLink } from "react-router-dom";
import { Button } from '@mui/material';
import { useTranslation } from "react-i18next";
import GlobeIcon from '../components/_common/GlobeIcon';
import i18next from 'i18next';
import SearchIcon from '../components/_common/SearchIcon';
import { connect } from 'react-redux';

const Header = (props:any) => {

  const languages = [
    { code: 'fr', name: 'Français', country_code: 'fr' },
    { code: 'en', name: 'English', country_code: 'gb' },
  ]

  const { t } = useTranslation();
  return (
    <React.Fragment>
      <AppBar
        position="static"
        color="default"
        elevation={0}
        sx={{ borderBottom: (theme) => `1px solid ${theme.palette.divider}` }}
      >
        <Toolbar sx={{ flexWrap: 'wrap' }}>
          <Typography variant="h6" color="inherit" noWrap sx={{ flexGrow: 1 }}>
            {t('app_title')}
          </Typography>
          <nav>
            <RouterLink to="/"> <Button >All Users</Button> </RouterLink>
            <RouterLink to="/Friends"> <Button >Friends</Button> </RouterLink>
            
          </nav>
          <div className="d-flex justify-content-end">
          <input name="searchInputBox" className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" onChange={(e) => props.onChangeSearchByName(e.target.value)} value={props.searchString}/>
            <button aria-label="user-finder" className="btn btn-link my-2 my-sm-0" type="button"><SearchIcon/></button>
          <div className="dropdown">
            <button aria-label="globe-language-selector" className="btn btn-link dropdown-toggle" type="button" id="globe-language-selector" data-bs-toggle="dropdown" aria-expanded="false" >
              <GlobeIcon />
            </button>
            <ul className="dropdown-menu" aria-labelledby="globe-language-selector">
              {languages.map(({ code, name, country_code }) => (
                <li key={country_code}><button className="dropdown-item" onClick={() => i18next.changeLanguage(code)}><span className={`flag-icon flag-icon-${country_code} mx-2`}></span>{name}</button></li>
              ))}
            </ul>
          </div>
        </div>
        </Toolbar>

      </AppBar>
    </React.Fragment>
  );
}

const mapStateToProps = (state: any) => {
  // console.log(state);
  return {
    searchString : state.searchString,
  };
};

const mapDispachToProps = (dispatch: any) => {
  return {
    onChangeSearchByName: (searchString: string) => { return dispatch({ type: "SEARCH_USER", value : searchString }) }
  };
};
export default connect(
  mapStateToProps,
  mapDispachToProps
)(Header);
