

import { AxiosResponse } from 'axios';
import { REHYDRATE } from 'redux-persist';
// import { REHYDRATE } from 'redux-persist';
import { takeEvery, put, delay, call, takeLatest, take} from 'redux-saga/effects';
import { getUsers } from '../server/api';

 
export interface IUser {
    id?: any | null,
    name: string,
    username: string,
    email?: string,
    friend?:boolean,
    circularProgressStatus?:string | 'idle'
  }

function* addFriendAsync(userId:any) {
    yield put({type:'ADD_FRIEND_ASYNC', value:userId});
}

export function* watchAddFriendAsync() {
    yield takeEvery('ADD_FRIEND', addFriendAsync)
}

// worker Saga: will be fired on USER_FETCH_REQUESTED actions
function* fetchUser() {
    try {
       const users : AxiosResponse<IUser[]> = yield call(getUsers);
       console.log('fetchUser called - ',users.data);
       yield put({type: "USER_FETCH_SUCCEEDED", value: users});
    } catch (e) {
        console.log('error', e);
       yield put({type: "USER_FETCH_FAILED", message: e});
    }
 }
 
 /*
   Alternatively you may use takeLatest.
 
   Does not allow concurrent fetches of user. If "USER_FETCH_REQUESTED" gets
   dispatched while a fetch is already pending, that pending fetch is cancelled
   and only the latest one will be run.
 */
export function* watchFetchUserSaga() {
    console.log('got dispatch USER_FETCH_REQUESTED');
    // yield take(REHYDRATE);
   yield takeEvery("USER_FETCH_REQUESTED", fetchUser);
 }
 
 function* searchUser(data:any) {
    yield put({type:'SEARCH_USER_ASYNC', value:data});
}

export function* watchSearchUserAsync() {
    yield takeEvery('SEARCH_USER', searchUser)
}

function* showSpinnerAsync(spinnerData:any) {
    yield put({type:'SHOW_SPINNER_ASYNC', value:spinnerData});
}

export function* watchShowSpinnerAsync() {
    yield takeEvery('SHOW_SPINNER', showSpinnerAsync)
}