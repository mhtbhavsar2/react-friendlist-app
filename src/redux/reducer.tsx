const initialState = {
    age: 20,
    showSpinner: 'idle',
    users: [],
    allUsers: [],
    userId: '',
    title: '',
    searchString: ''
};

const reducer = (state = initialState, action: any) => {
    const newState = { ...state };

    switch (action.type) {
        case 'USER_FETCH_SUCCEEDED':
             newState.users = action.value.data;
             newState.allUsers = action.value.data;
            break;
        case 'ADD_FRIEND_ASYNC':
            newState.userId = action.value.value;
            newState.users.map((user: any) => {
                if (newState.userId === user.id) {
                    console.log('updated user -', newState.userId, user)
                    user.friend = true;
                    user.circularProgressStatus = 'success';
                }
            })
            let usersList: never[] = [];
            newState.users.forEach(element => {
                usersList.push(element)
            });
            return { ...newState, users: usersList, userId: newState.userId, allUsers : usersList };

        case 'SEARCH_USER_ASYNC':
            newState.searchString = action.value.value;
            console.log('newState.searchString - ', newState.searchString);
            break;
        case 'SHOW_SPINNER_ASYNC':
            console.log('SHOW_SPINNER_ASYNC', action.value.value.showSpinner);
            newState.showSpinner = action.value.value.showSpinner;
            newState.userId = action.value.value.userId;
            console.log('newState.searchString - ', newState.searchString);
            break;
    }
    return newState;
};

export default reducer;