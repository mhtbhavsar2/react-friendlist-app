import React, { Suspense } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import LanguageDetector from 'i18next-browser-languagedetector';
import 'bootstrap/dist/js/bootstrap.js';
import resourcesToBackend from 'i18next-resources-to-backend';
import { PersistGate } from 'redux-persist/integration/react'

import App from './components/App/App';
import Spinner from './components/_common/Spinner';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'flag-icon-css/css/flag-icons.min.css'

import rootReducer from './redux/reducer';
import { createStore, applyMiddleware } from 'redux';
import { watchAddFriendAsync, watchSearchUserAsync, watchFetchUserSaga, watchShowSpinnerAsync } from './redux/sagas';
import createSagaMiddleware from 'redux-saga';

import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'

const persistConfig = {
  key: 'root',
  storage,
}

const sagaMiddleware = createSagaMiddleware();
const persistedReducer = persistReducer(persistConfig, rootReducer)
const store = createStore(persistedReducer, applyMiddleware(sagaMiddleware));
const persistor = persistStore(store)

sagaMiddleware.run(watchAddFriendAsync);
sagaMiddleware.run(watchFetchUserSaga);
sagaMiddleware.run(watchSearchUserAsync);
sagaMiddleware.run(watchShowSpinnerAsync);

i18n
  .use(initReactI18next) 
  .use(resourcesToBackend((language, namespace, callback) => {
    import(`./locales/${language}/translation.json`)
      .then((resources) => {
        callback(null, resources)
      })
      .catch((error) => {
        callback(error, null)
      })
  }))
  .use(LanguageDetector)
  .init({
    supportedLngs: ['en', 'fr'],
    fallbackLng: "en",

    interpolation: {
      escapeValue: false
    },
    detection: {
      order: ['cookie', 'localStorage', 'sessionStorage', 'path', 'navigation', 'subdomain'],
      caches: ['cookie'],
    },
  });

ReactDOM.render(
  <Suspense fallback={<Spinner />}>
    <React.StrictMode>
      <Provider store={store}>
        <PersistGate loading={<Spinner />} persistor={persistor}>
          <App />
        </PersistGate>
      </Provider>
    </React.StrictMode>
  </Suspense>
  ,
  document.getElementById('root')
);

